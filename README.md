# Settings

## Install VSCode into linux

```
git clone https://gitlab.com/shinlan/visual-studio-code-config ~/Downloads
sudo ~/Downloads/visual-studio-code-config/install.sh
```

[Download Visual Studio Code] (https://code.visualstudio.com/Download)

```bash
sudo tar -xzf ~/Downloads/code-stable-x64-1683731267.tar.gz -C /opt/
sudo ln -s /opt/VSCode-linux-x64/bin/code /usr/local/bin/code
sudo vim /usr/share/applications/code.desktop
```

### Code.desktop

```
[Desktop Entry]
Name=Visual Studio Code
GenericName=Text Editor
Comment=Code Editing, Redefined
Exec=/usr/local/bin/code %U
Icon=/opt/VSCode-linux-x64/resources/app/resources/linux/code.png
Type=Application
StartupNotify=true
Categories=Utility;TextEditor;Development;IDE
```

---

## Extensions

- Angular 17 Snippets
- Angular Snippets (Version 16) _Disable_
- Angular Language Service
- Auto Rename Tag
- Backticks
- Console Ninja
- Database client JDBC (dependences) (Disable)
- Git History
- GitLens - Git supercharged
- Go
- JavaScript (ES6) code snippets
- npm Intellisense
- Path Intellisense
- Prettier - Code formatter
- Python
- Python Enviroment Manager
- Python Extension Pack
- Python Indent
- TODO Highlight
- IntelliCode
- IntelliCode Api Usage Example
- MySQL (Disable)
- Polacode
- Postman (Disable)
- Svg Preview
- ESLint
- Font Switcher

---

## Themes

- Nord
- **Nord Deep**
- Horizon Theme
- Linux Themes for VS Code
- Bearded Theme
- Catppuccin for VSCode
- Cotion Theme

---

## Icon

- Bearded Icons
- **Fluent Icons**
- vscode-icons
- Material Icon Theme
- Bearded Icons
- Catppuccin Icons for VSCode
- **Material Theme Icons**
