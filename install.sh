sudo pacman -S --needed curl tar ttf-nerd-fonts-symbols ttf-jetbrains-mono-nerd ttf-jetbrains-mono noto-fonts-cjk noto-fonts-emoji awesome-terminal-fonts

curl -o ~/Downloads/vscode.tar.gz -L "https://code.visualstudio.com/sha/download?build=stable&os=linux-x64"
sudo tar -xzf ~/Downloads/vscode.tar.gz -C /opt/
sudo ln -s /opt/VSCode-linux-x64/bin/code /usr/local/bin/code
echo "[Desktop Entry]
Name=Visual Studio Code
GenericName=Text Editor. Refined.
Comment=Code Editing, Redefined
Exec=/usr/local/bin/code --unity-launch %F
Icon=/opt/VSCode-linux-x64/resources/app/resources/linux/code.png
Type=Application
StartupWMClass=Code
StartupNotify=true
Categories=TextEditor;Development;IDE;
MimeType=text/plain;inode/directory;application/x-code-workspace;
Actions=new-empty-window;
Keywords=vscode;


[Desktop Action new-empty-window]
Name=New Empty Window
Exec=/usr/local/bin/code --new-window %F
Icon=/opt/VSCode-linux-x64/resources/app/resources/linux/code.png" | sudo tee /usr/share/applications/code.desktop

echo "[Desktop Entry]
Name=Visual Studio Code - URL Handler
Comment=Code Editing. Redefined.
GenericName=Text Editor
Exec=/usr/local/bin/code --open-url %U
Icon=/opt/VSCode-linux-x64/resources/app/resources/linux/code.png
Type=Application
NoDisplay=true
StartupNotify=true
Categories=TextEditor;Development;IDE;
MimeType=x-scheme-handler/vscode;" | sudo tee /usr/share/applications/code-url-handler.desktop



sudo mkdir /usr/share/fonts/VsCode/
sudo cp ~/Downloads/visual-studio-code-config/fonts/* /usr/share/fonts/VsCode/
sudo fc-cache -f -v

code ~/Downloads/visual-studio-code-config/